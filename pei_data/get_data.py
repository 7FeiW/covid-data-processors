import json
import datetime
import urllib.request

def get_and_process_reports_by_province(province):
    covid_tracker_api = 'https://api.covid19tracker.ca/reports/province/{}?per_page=10000'.format(province)
    #print(covid_tracker_api)
    with urllib.request.urlopen(covid_tracker_api) as url:
        data = json.loads(url.read().decode())
        with open('{}_reports.json'.format(province), 'w+') as outfile:
            json.dump(data, outfile)
        
    counts = {} 

    for data_object in data['data']:
        date = data_object["date"].split()[0]
        #print(data_object)
        
        daily_cases = data_object['change_cases'] if data_object['change_cases'] != None else 0
        daily_tests = data_object['change_tests'] if data_object['change_cases'] != None else 0
        daily_fatalities = data_object['change_fatalities'] if data_object['change_fatalities'] != None else 0
        daily_hospitalizations = data_object['change_hospitalizations'] if data_object['change_hospitalizations'] != None else 0
        daily_criticals = data_object['change_criticals'] if data_object['change_criticals'] != None else 0
        
        counts[date] = {'daily_cases': daily_cases , 'daily_tests':daily_tests, 'daily_fatalities':daily_fatalities, 'daily_hospitalizations': daily_hospitalizations, 'daily_criticals' : daily_criticals}
    

    date = datetime.datetime(2020, 2, 14)
    end_date = datetime.datetime(2021, 1, 25)
    
    print('{},{},{},{},{},{}'.format('date','daily_cases','daily_tests', 'daily_fatalities', 'daily_hospitalizations', 'daily_criticals'))
    while date != end_date:
        date_str = date.strftime("%Y-%m-%d")
        if date_str in counts:
            print('{},{},{},{},{},{}'.format(date_str,counts[date_str]['daily_cases'], counts[date_str]['daily_tests'],
                  counts[date_str]['daily_fatalities'],counts[date_str]['daily_hospitalizations'],counts[date_str]['daily_criticals']))
        else:
            print('{},N/A,N/A,N/A,N/A,N/A'.format(date_str))
        date += datetime.timedelta(days=1)
    
def get_and_processs(data_type):
    covid_tracker_api = 'https://api.covid19tracker.ca/{}?province=PE&per_page=10000'.format(data_type)
    with urllib.request.urlopen(covid_tracker_api) as url:
        data = json.loads(url.read().decode())
        with open('{}.json'.format(data_type), 'w+') as outfile:
            json.dump(data, outfile)
        
    counts = {} 
    for case in data['data']:
        date = case["date"].split()[0]
        if date not in counts:
            counts[date] = 0
        counts[date] += 1


    date = datetime.datetime(2020, 2, 14)
    end_date = datetime.datetime(2021, 1, 25)
    while date != end_date:
        date_str = date.strftime("%Y-%m-%d")
        if date_str in counts:
            print(counts[date_str])
        else:
            print(0)
        date += datetime.timedelta(days=1)

#older code
#print('case data')
#get_and_processs('cases')
#print('fatalities data')
#get_and_processs('fatalities')

get_and_process_reports_by_province('pe')