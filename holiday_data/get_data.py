# /usr/bin/env python3
# encoding: utf-8

import os
import re
import datetime
import csv
import urllib.request

# [date, summary]
def read_ics(ics_file):
    data = {}
    date = None
    summary = None
    for line in open(ics_file,  encoding="utf8"):
        line = line.rstrip("\r\n")
        if line == "BEGIN:VEVENT":
            continue
        elif line.startswith("DTSTART;"):
            m = re.match("^DTSTART;VALUE=DATE:(\d{4}\d{2}\d{2})$", line)
            date = datetime.datetime.strptime(m.group(1), "%Y%m%d").date()
        elif line.startswith("SUMMARY;LANGUAGE=en-us:"):
            m = re.match("^SUMMARY;LANGUAGE=en-us:(.*)$", line)
            summary = m.group(1)
        elif line == "END:VEVENT" and "regional" not in summary.lower():
                date_str = date.strftime("%Y-%m-%d")
                if date_str in data:
                    data[date.strftime("%Y-%m-%d")]  += ',' + summary
                else:
                    data[date.strftime("%Y-%m-%d")]  = summary
    return data

def write_result_csv(csv_file, ics_data):
    with open(csv_file, "w", newline='',encoding='utf-8' ) as o:
        writer = csv.writer(o)
        start_data = datetime.datetime(2020,1,1)
        end_data = datetime.datetime(2021,6,1)
        while start_data < end_data:
            date_str = start_data.strftime("%Y-%m-%d")
            summary = ''
            if date_str in ics_data:
                summary = ics_data[date_str]
            writer.writerow([date_str, summary])
            start_data =  start_data + datetime.timedelta(days = 1)
            #print(start_data.strftime("%Y-%m-%d"))
        #exit()
        #for date, summary in rows:
        

def download(url, ics_file):
    f = urllib.request.urlopen(url)
    ics = f.read()
    f.close()
    
    o = open(ics_file, "wb")
    o.write(ics)
    o.close()

def get_data(urls_dict, results_dir):
    for location in urls_dict:
        url = urls_dict[location]
        ics_file = os.path.join('./ics_cache',"{}.ics".format(location))
        print(location, url)
        download(url, ics_file)
        if os.path.exists(ics_file):
            ics_data = read_ics(ics_file)
            csv_file = os.path.join(results_dir, '{}.csv'.format(location))
            write_result_csv(csv_file, ics_data)

def read_url_list(list_file, filter_list):
    urls_dict = {}
    filters = []
    if filter_list is not None:
        with open(filter_list) as filter_csv:
            reader = csv.reader(filter_csv)
            for row in reader:
                filters.append(row[0].rstrip())
    
    with open(list_file) as csv_file:
        reader = csv.reader(csv_file)
        for row in reader:
            country = row[0].rstrip()
            url = row[1].rstrip()
            if filter_list is not None:
                if  country in filters:
                    urls_dict[country] = url
            else:
                urls_dict[country] = url
    return urls_dict

def get_states_url_list(list_file):
    base_url = 'https://www.officeholidays.com/ics-clean/usa/{}'
    urls_dict = {}
    with open(list_file) as csv_file:
        reader = csv.reader(csv_file)
        for row in reader:
            state = row[0].rstrip()
            urls_dict[state] = base_url.format(state.lower().replace(' ','-'))
    return urls_dict

if __name__ == "__main__":
    urls_dict = read_url_list('country_list.csv', 'x_prize_countries.csv')
    get_data(urls_dict, './results/countries')
    
    urls_dict = read_url_list('us_states.csv', None)
    get_data(urls_dict, './results/us_states')


    urls_dict = read_url_list('uk_countries.csv', None)
    get_data(urls_dict, './results/uk_countries')

