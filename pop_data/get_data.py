# /usr/bin/env python3
# encoding: utf-8

import os
import re
import datetime
import csv

def get_x_prize_countries(filename):
    countreis = {}
    with open(filename) as country_csv:
        reader = csv.reader(country_csv)
        for row in reader:
            country_name = row[0].rstrip()
            country_code = row[1].rstrip()
            countreis[country_code] = country_name
    return countreis

def process_data(filename, countries_dict):
    
    results = {}
    with open(filename) as data_csv:
        reader = csv.reader(data_csv)
        for idx,row in enumerate(reader):
            # some hard coding, but this is a one off
            if idx < 5:
                continue 
            
            country_code = row[1].rstrip()
            if country_code not in countries_dict:
                continue
            
            country_name = countries_dict[country_code]
            results[country_code] = 0
            for value in reversed(row):
            
                if value != '':
                    try:
                        float(value)
                    except ValueError:
                        print("can't find value for", country_name)
                    else:
                        results[country_name] = float(value)
                    break
    return results        
    #with open(output_filename, 'w+', encoding='UTF-8') as outfile:
    #    for country_code in results:
    #        outfile.write('{},{},{}\n'.format(countries_dict[country_code],country_code,results[country_code]))

if __name__ == "__main__":
    
    countries = get_x_prize_countries('./data/x_prize_countries.csv')
        
    #gdp = process_data('./data/API_NY.GDP.MKTP.CD_DS2_en_csv_v2_1926685.csv', countries)
    population_density = process_data('./data/API_EN.POP.DNST_DS2_en_csv_v2_1927453.csv', countries)
    urban_population = process_data('./data/API_SP.URB.TOTL.IN.ZS_DS2_en_csv_v2_1929300.csv', countries)
    
    #output_data([population_density, urban_population],['population density per km2', 'urban population %'],'./results/output.csv')
    
    output_filename = './results/output.csv'
    data_objects, data_labels = [population_density, urban_population],['population density per km2', 'urban population %']
    
    with open('./data/Countries_regions.csv', encoding='UTF-8') as  template_csv, open(output_filename, 'w+', encoding='UTF-8') as outfile:
        reader = csv.reader(template_csv)
        for idx,row in enumerate(reader):
            # some hard coding, but this is a one off
            if idx == 0:
                outfile.write('{},{}'.format(row[0], row[1])) 
                for label in data_labels:
                    outfile.write(',{}'.format(label))
                outfile.write('\n')
                continue
            
            country_name = row[0]
            region_name = row[1]
            data_str = ''
            #print(country_name, region_name)
            if region_name == '':
                for data in data_objects:
                    value =  data[country_name] if country_name in data else ''
                    data_str += ',{}'.format(value)
                outfile.write('{},{}{}\n'.format(country_name, region_name, data_str))
            else:
                 outfile.write('{},{}\n'.format(country_name, region_name))
