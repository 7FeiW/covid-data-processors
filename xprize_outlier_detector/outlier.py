import csv
import numpy as np
from scipy.signal import argrelextrema
import datetime
import os
 
def compute(data, country_or_region_code):
    R0 = 5.7
    cor_daily_cases = np.ediff1d(np.array(data[country_or_region_code]['case']))
    cor_daily_cases = np.insert(cor_daily_cases, 0, data[country_or_region_code]['case'][0], axis=0)
    # fill negative with 0, this can occurs for the last row or rows in the data
    # where latest data is not avaibale 
    cor_daily_cases[cor_daily_cases < 0] = 0
    
    first_order = np.gradient(cor_daily_cases, 1)
    second_order = np.gradient(first_order, 1)
    
    peak_idx = argrelextrema(cor_daily_cases, np.greater)[0] 

    cor_daily_cases_outlier_removed = np.copy(cor_daily_cases)
    for x in peak_idx:    
        if cor_daily_cases_outlier_removed[x-1] == 0 or cor_daily_cases_outlier_removed[x] < 50:
            continue
        if  cor_daily_cases_outlier_removed[x]/cor_daily_cases_outlier_removed[x-1]  > R0:
            fix_value = (cor_daily_cases_outlier_removed[x-1] + cor_daily_cases_outlier_removed[x+1])/2
            #print('outlier fix at {} ({}->{})'.format(x, cor_daily_cases[x], fix_value))
            cor_daily_cases_outlier_removed[x] = fix_value
            if fix_value < 0:
                print('Error Outlier fix at {} ({}->{})'.format(x, cor_daily_cases_outlier_removed[x], fix_value))
 
    first_order_outlier_removed = np.gradient(cor_daily_cases, 1)
    second_order_outlier_removed = np.gradient(first_order_outlier_removed, 1)
    return cor_daily_cases, cor_daily_cases_outlier_removed, first_order, second_order, first_order_outlier_removed, second_order_outlier_removed

def get_wanted_nams(filename):
    wanted =[]
    with open(filename) as filter_csv:
        reader = csv.reader(filter_csv)
        for row in reader:
            wanted.append(row[0].rstrip())
    return wanted

def read_oxcgrt(filename):
    data = {}
    with open(filename) as oxcgrt_in:
        csv_reader = csv.reader(oxcgrt_in)
        cname_idx, rname_idx, cc_idx, cd_idx, date_idx = 0,0,0,0,0
        for idx,row in enumerate(csv_reader):
            if idx == 0:
                cname_idx = row.index('CountryName')
                rname_idx = row.index('RegionName')
                cc_idx = row.index('ConfirmedCases')
                cd_idx = row.index('ConfirmedDeaths')
                date_idx = row.index('Date')
            else:
                cname = row[cname_idx]
                rname = row[rname_idx]
                case = int(row[cc_idx]) if row[cc_idx] != '' else 0 
                death = int(row[cd_idx]) if row[cd_idx] != '' else 0 
                #date = row[date_idx]
            
                key = cname if rname == '' else rname
                if key not in data:
                    data[key] = {}
                    data[key]['case'] = []

                data[key]['case'].append(case)
                #data[key]['death'].append(death)
    return data

def process_data(data, wanted_country_or_region, sub_dir = None):
    
    results_dir = 'outlier_resutls' if sub_dir == None else os.path.join('outlier_resutls', sub_dir)
    os.makedirs(results_dir, exist_ok= True)
    
    for country_or_region in wanted_country_or_region:
        print(country_or_region)
        cor_daily_cases, cor_daily_cases_outlier_removed, first_order, second_order, first_order_outlier_removed, second_order_outlier_removed =  compute(data, country_or_region)
        with open(os.path.join(results_dir,'{}.csv'.format(country_or_region)), 'w+', encoding='utf-8') as outfile:
            start_data = datetime.datetime(2020,1,1)
            outfile.write('date, confirmed_daily_cases, first_order,second_order, confirmed_daily_cases_outlier_removed,first_order_outlier_removed,second_order_outlier_removed\n')
            idx = 0
            while idx < len(cor_daily_cases_outlier_removed):
                date_str = start_data.strftime("%Y-%m-%d")
                outfile.write('{},{},{},{},{},{}\n'.format(date_str, cor_daily_cases[idx], first_order[idx], second_order[idx], cor_daily_cases_outlier_removed[idx], first_order_outlier_removed[idx], second_order_outlier_removed[idx]))
                start_data =  start_data + datetime.timedelta(days = 1)
                idx += 1
            #outfile.write(start_data.strftime("%Y-%m-%d")+',,,,,\n')
                 
if __name__ == "__main__":
    data = read_oxcgrt(filename = 'OxCGRT_latest.csv')
    countries = get_wanted_nams('x_prize_countries.csv')
    us_states = get_wanted_nams('x_prize_us_states.csv')
    uk_countries = get_wanted_nams('x_prize_uk_countries.csv')
    #print(counrties)
    process_data(data,countries, 'countries')
    process_data(data,us_states, 'us_states')
    process_data(data,uk_countries, 'uk_countries')
